#!/bin/bash

mkdir -p logs
exec > >(tee -a logs/provision.log)
exec 2> >(tee -a logs/provision.err >&2)

install_or_update_dotfiles() {
  git -C $HOME/.dotfiles pull || git clone https://github.com/pcuci/dotfiles.git $HOME/.dotfiles
  rm -f $HOME/.profile
  rm -f $HOME/.bash_logout
  cd $HOME/.dotfiles && ./install
}

add_authorized_key() {
  mkdir -p $HOME/.ssh
  chmod 700 $HOME/.ssh
  grep -qxF "$SSH_KEY" $HOME/.ssh/authorized_keys || echo "$SSH_KEY" >> $HOME/.ssh/authorized_keys
  chmod 600 $HOME/.ssh/authorized_keys
}

ansible_provision() {
  git -C $HOME/sun pull || git clone https://gitlab.com/deposition.cloud/infra/compute/sun.git $HOME/sun
  cd $HOME/sun
  ansible-galaxy collection install -r requirements.yaml
  ansible-galaxy role install -r requirements.yaml --roles-path roles/
  ansible-playbook local.yml --tags docker
}

wait_for_apt_lock() {
  timeout=60  # 1 minute timeout
  elapsed=0   # Time elapsed

  while sudo fuser /var/lib/dpkg/lock-frontend /var/lib/apt/lists/lock >/dev/null 2>&1; do
    if [ $elapsed -ge $timeout ]; then
      echo "Timed out after $timeout seconds of waiting for other software managers to finish."
      exit 1
    fi
    echo "Waiting for other software managers to finish..."
    sleep 5
    elapsed=$((elapsed + 5))
  done
}

# Function to check and append module if it's not already present
check_and_append_module() {
    local module=$1
    if ! grep -q "^${module}$" /etc/modules; then
        echo $module | sudo tee -a /etc/modules
    else
        echo "Module $module is already present"
    fi
}

# Main Script
main() {
  set -x
  set -e

  echo "Running on $(hostname)"

  # Load kernel modules for Ceph to work
  sudo modprobe rbd
  sudo modprobe ceph

  # Ensure `ceph` and `rbd` modules load up after reboot
  check_and_append_module ceph
  check_and_append_module rbd

  # Wait in case cloud init hasn't finished yet
  wait_for_apt_lock

  # Update and upgrade packages
  sudo apt-get update 
  sudo apt-get upgrade -y

  # Install essential packages
  sudo apt-get install -y --no-install-recommends at curl git keychain vim ansible

  # Setup root dotfiles
  install_or_update_dotfiles

  # Add authorized key for root
  add_authorized_key

  # Ansible provision
  ansible_provision

  # Disable debugging output
  set +x
}

# Execute main script
main
