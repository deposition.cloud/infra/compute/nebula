#!/bin/bash

set -e

mkdir -p logs
exec > >(tee -a logs/delete_template.log)
exec 2> >(tee -a logs/delete_template.err >&2)

set -x

# Check for required packages
if ! dpkg -l | grep -qw "qemu-server"; then
  echo "Proxmox command-line tool 'qm' is required but not installed. Exiting."
  exit 1
fi

for var in "VMID"; do
  [ -z "${!var}" ] && { echo "Error: $var is undefined."; exit 1; }
done

# Check if VM or template exists
if qm list | grep -qw "$VMID"; then
  # Delete the VM or template
  qm destroy "$VMID"
  echo "Deleted VM or template with ID $VMID."
else
  echo "No VM or template found with ID $VMID."
fi

set +x

echo "Template and associated resources deleted successfully!"
