#!/bin/bash

set -e

mkdir -p logs
exec > >(tee -a logs/create_template.log)
exec 2> >(tee -a logs/create_template.err >&2)

set -x

# Check and install required packages on the PVE host
for package in "libguestfs-tools" "wget" "jq"; do
  dpkg -l | grep -qw $package || apt install $package --yes
done

for var in "VMID" "VM_NAME" "DOWNLOAD_URL"; do
  [ -z "${!var}" ] && { echo "Error: $var is undefined."; exit 1; }
done

: ${MEMORY:=2048}
: ${CORES:=3}
: ${DOWNLOADS_DIR:="$HOME/downloads"}
mkdir -p $DOWNLOADS_DIR

IMG_NAME=$(basename $DOWNLOAD_URL)
IMG_PATH="$DOWNLOADS_DIR/$IMG_NAME"

# Download Ubuntu cloud-init disk image if it doesn't exist
if [ ! -f "$IMG_PATH" ]; then
  wget -nc -P "$DOWNLOADS_DIR" "$DOWNLOAD_URL"
else
  echo "File $IMG_NAME already exists in $DOWNLOADS_DIR. Skipping download."
fi

# Customize the disk image
virt-customize -a $IMG_PATH --install qemu-guest-agent

# Check if VM already exists
if ! qm list | grep -qw "$VMID"; then
  # Create a new VM in Proxmox
  qm create $VMID --name "$VM_NAME" --memory $MEMORY --cores $CORES --net0 virtio,bridge=vmbr0
fi

# Check if disk is already imported
if ! qm config $VMID | grep -qw "scsi0"; then
  # Import the disk to the VM
  qm importdisk $VMID $IMG_PATH local-lvm
fi

# Attach the disk to the VM and set other configurations
qm set $VMID --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-$VMID-disk-0
qm set $VMID --boot c --bootdisk scsi0
qm set $VMID --ide2 local-lvm:cloudinit
qm set $VMID --serial0 socket --vga serial0
qm set $VMID --agent enabled=1

# Convert the VM to a template
qm template $VMID

set +x

echo "VM template created successfully!"
