import * as pulumi from "@pulumi/pulumi";
import * as proxmox from "@muhlba91/pulumi-proxmoxve";
import { readFile } from "./src/readFile"
import { remote, types } from "@pulumi/command";
import { hash } from "./src/hashFile";
import { VM, workdir, prefix } from "./src/config"
import { ProxmoxVirtualMachine } from "./src/vm"
import { OpenWRT } from './src/router'
import path = require("path");
import { Lease, getRandomMacFromName as getRandomMacForName } from "./src/router/lease";

let config = new pulumi.Config()

const endpoint = config.require("endpoint");
const username = config.require("username");
const password = config.requireSecret("password");
const routerPassword = config.requireSecret("routerPassword");
const host = config.require("host");
const gateway = config.require("gateway");

const vm = config.requireObject<VM>('vm')

const templateVmId = vm.template.id
const templateVmName = prefix(vm.template.name)
const templateVmDownloadUrl = vm.template.downloadUrl

const privateKeyPath = config.require("privateKeyPath");
const publicKeyPath = config.require("publicKeyPath");

const privateKey = readFile(privateKeyPath);
const publicKey = readFile(publicKeyPath);

const proxmoxve = new proxmox.Provider(prefix('proxmoxve'), {
  endpoint,
  username,
  password,
  insecure: true
});

let pveNodes = proxmox.cluster.getNodes({
  provider: proxmoxve
})

const pveConnection: types.input.remote.ConnectionArgs = {
  host,
  user: "root",
  password,
  privateKey,
  privateKeyPassword: password
};

const createTemplateScriptPath = "scripts/create_template.sh"
const deleteTemplateScriptPath = "scripts/delete_template.sh"

const workdirCommand = new remote.Command(prefix("workdir"), {
  connection: pveConnection,
  create: pulumi.interpolate`mkdir -p $HOME/${workdir}`,
  update: pulumi.interpolate`mkdir -p $HOME/${workdir}`,
}, {
  parent: proxmoxve,
  dependsOn: proxmoxve
});

const deleteTemplateScript = new remote.CopyFile(prefix(`deleteTemplateScript-${hash(deleteTemplateScriptPath)}`), {
  connection: pveConnection,
  localPath: deleteTemplateScriptPath,
  remotePath: `${workdir}/delete_template.sh`,
}, {
  parent: workdirCommand,
  dependsOn: workdirCommand
})

const createTemplateScript = new remote.CopyFile(prefix(`createTemplateScript-${hash(createTemplateScriptPath)}`), {
  connection: pveConnection,
  localPath: createTemplateScriptPath,
  remotePath: `${workdir}/create_template.sh`,
}, {
  parent: workdirCommand,
  dependsOn: workdirCommand
})

// Check for newer images here: https://cloud-images.ubuntu.com/
const vmTemplate = new remote.Command(prefix("vmTemplate"), {
  connection: pveConnection,
  create: pulumi.interpolate`
    export VMID=${templateVmId}
    export VM_NAME=${templateVmName}
    export DOWNLOAD_URL=${templateVmDownloadUrl}
    cd ~/${workdir}
    bash create_template.sh`,
  delete: pulumi.interpolate`
    export VMID=${templateVmId}
    cd ~/${workdir}
    bash delete_template.sh`,
  triggers: [createTemplateScript.id]
}, {
  parent: createTemplateScript,
  dependsOn: [createTemplateScript, deleteTemplateScript]
});


const { nodes, leases } = pulumi.unsecret(routerPassword).apply(routerPassword => {
  const nodes: pulumi.Output<ProxmoxVirtualMachine>[] = []
  const leases: Lease[] = []

  const router = new OpenWRT('router', {
    range: {
      start: vm.range.start,
      limit: vm.range.limit
    },
    gateway: {
      host: gateway,
      username: 'root',
      password: routerPassword,
    }
  }, {})
  
  const provisionScriptPath = path.resolve(__dirname, "scripts/provision.sh");

  for (let deployment of vm.deployments ?? []) {
    const vmBaseName = deployment.name
  
    for (let node of deployment.nodes) {
      for (let replica = 0; replica < (node.replicas ?? 1); ++replica) {
        const resourceName = prefix(`${vmBaseName}-${node.name}-${replica}`)

        const lease: Lease = router.nextLease(getRandomMacForName(resourceName), `${resourceName}`);

        leases.push(lease)
  
        const vm = pulumi.all([lease.address, lease.mac]).apply(([ip, mac]) => {
          const vm = new ProxmoxVirtualMachine(resourceName, {
            password,
            provisionScriptPath,
            publicKey,
            pveConnection,
            vmName: resourceName,
            pveNode: node.name,
            templateVmId,
            network: {
              ip,
              mac,
              gateway
            }
          }, {
            provider: proxmoxve,
            parent: vmTemplate,
            dependsOn: [vmTemplate, lease]
          })
  
          return vm
        })

        nodes.push(vm)
      }
    }
  }

  return {
    leases,
    nodes
  }
})

export const out = {
  nodes,
  leases
}