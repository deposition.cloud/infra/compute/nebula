import { ComponentResource, ComponentResourceOptions, Output } from "@pulumi/pulumi";
import * as pulumi from "@pulumi/pulumi";
import { remote, types } from "@pulumi/command";
import * as proxmox from "@muhlba91/pulumi-proxmoxve";
import { workdir } from "../config"
import { hash } from "../hashFile";
import { live, stack } from "../config"

export interface ProxmoxVirtualMachineArgs {
  vmName: string
  templateVmId: number
  password: Output<string>
  publicKey: string
  pveNode: string
  provisionScriptPath: string
  pveConnection: types.input.remote.ConnectionArgs,
  network: Network
}

export class ProxmoxVirtualMachine extends ComponentResource {
  vm: proxmox.vm.VirtualMachine;
  createWorkdir: remote.Command;
  copyProvisionScript: remote.CopyFile;
  provision: remote.Command;
  reboot: remote.Command;

  constructor(name: string, args: ProxmoxVirtualMachineArgs, opts?: ComponentResourceOptions) {
    super("nebula:vm:ProxmoxVirtualMachine", name, args, opts);

    const {
      vmName,
      templateVmId,
      password,
      publicKey,
      pveNode,
      provisionScriptPath,
      pveConnection,
      network
    } = args

    this.vm = new proxmox.vm.VirtualMachine(vmName, {
      ...getBaseConfig(vmName, network, pveNode, templateVmId, 'pve', password, publicKey),
    }, {
      parent: this,
      dependsOn: this
    })

    const connection: types.input.remote.ConnectionArgs = {
      ...pveConnection,
      user: "paul",
      host: network.ip
    }

    this.createWorkdir = new remote.Command(`${vmName}-vmWorkdir`, {
      connection,
      create: pulumi.interpolate`mkdir -p $HOME/${workdir}`,
      update: pulumi.interpolate`mkdir -p $HOME/${workdir}`,
      // delete: `sleep 5 ; sudo dhclient -r" | at now`, // release the IP to clean up ourselves, otherwise the router gets confused
      triggers: [
        this.vm
      ]
    }, {
      parent: this.vm,
      dependsOn: this.vm
    });

    this.copyProvisionScript = new remote.CopyFile(`${vmName}-provisionScript-${hash(provisionScriptPath)}`, {
      connection,
      localPath: provisionScriptPath,
      remotePath: `${workdir}/provision.sh`,
      triggers: [
        this.vm
      ]
    }, {
      parent: this.createWorkdir,
      dependsOn: this.createWorkdir
    })

    this.provision = new remote.Command(`${vmName}-provision`, {
      connection: connection,
      create: pulumi.interpolate`
        export SSH_KEY="${publicKey}"
        export ANSIBLE_BECOME_PASS="${password}"
        cd ~/${workdir}
        bash provision.sh`,
      triggers: [
        this.vm,
        this.copyProvisionScript.id
      ]
    }, {
      parent: this.copyProvisionScript,
      dependsOn: this.copyProvisionScript
    });

    this.reboot = new remote.Command(`${vmName}-reboot`, {
      connection: connection,
      create: pulumi.interpolate`
        sudo dhclient -r
        sudo dhclient
        echo "sleep 3 ; sudo shutdown -r now" | at now`,
      triggers: [
        this.vm,
        this.provision
      ]
    }, {
      parent: this.provision,
      dependsOn: this.provision
    });

    this.registerOutputs({
      provision: this.provision,
    });
  }
}

export interface Network {
  ip: string,
  mac: string,
  gateway: string
} 

const getBaseConfig = (name: string, network: Network, nodeName: string, templateVmId: number, templateSourceNodeName: string, password: pulumi.Output<string>, publicKey: string) => {
  const baseVirtualMachineArgs: proxmox.vm.VirtualMachineArgs = {
    nodeName,
    agent: {
      enabled: true,
      trim: true,
      type: 'virtio',
    },
    bios: 'seabios',
    cpu: {
      cores: (stack === live) ? 6 : 4,
      sockets: 1,
    },
    clone: {
      vmId: templateVmId,
      datastoreId: 'local-lvm',
      nodeName: templateSourceNodeName,
      full: true,
    },
    disks: [{
      interface: 'scsi0',
      datastoreId: 'local-lvm',
      fileFormat: 'raw',
      size: 60,
    }],
    memory: {
      dedicated: (stack === live) ? 32768 : 16384,
    },
    name,
    networkDevices: [
      {
        bridge: 'vmbr0',
        model: 'virtio',
        macAddress: network.mac
      },
    ],
    onBoot: true,
    operatingSystem: {
      type: 'l26',
    },
    tags: [stack],
    initialization: {
      type: 'nocloud',
      datastoreId: 'local-lvm',
      ipConfigs: [{
        ipv4: {
          address: `${network.ip}/24`,
          gateway: network.gateway
        }
      }],
      userAccount: {
        username: 'paul',
        password,
        keys: [publicKey],
      },
    },
  }

  return baseVirtualMachineArgs
}
