import * as fs from "fs";

export function readFile (filePath: string) {
  try {
    return fs.readFileSync(filePath).toString("utf8").trim();
  } catch (error) {
    throw new Error(`Failed to read file: ${filePath}`);
  }
}

