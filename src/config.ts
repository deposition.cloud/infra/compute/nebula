import * as pulumi from "@pulumi/pulumi";

export const live = 'live' // what do we call the live production stack?

export const org = pulumi.getOrganization()
export const project = pulumi.getProject()
export const stack = pulumi.getStack()

export const workdir = `${org}-${project}-${stack}`

export const prefix = function (resourceName: string): string {
  if (stack === live) {
    return resourceName;
  }

  return `${stack}-${resourceName}`;
}

export interface VM {
  template: Template
  range: Range
  deployments: Deployment[]
}

export interface Template {
  id: number
  name: string
  downloadUrl: string
}

export interface Range {
  start: string
  limit: number
}

export interface Deployment {
  name: string
  nodes: Node[]
}

export interface Node {
  name: string
  replicas?: number
}
