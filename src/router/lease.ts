import * as pulumi from "@pulumi/pulumi";
import { CreateResult, UpdateResult, ReadResult, DiffResult } from "@pulumi/pulumi/dynamic";
import { NodeSSH } from 'node-ssh';
import * as path from 'path';
import ip from 'ip';
import seedrandom from 'seedrandom'

const dhcpLuaScriptPath = path.resolve(__dirname, 'scripts/dhcp.lua');

export enum LeaseState {
  Assigned = "Assigned",
  Requested = "Requested",
  Unassigned = "Unassigned"
}

export interface Gateway {
  host: string,
  port?: 22,
  username: string,
  password: string,
}

interface LeaseProviderInputs {
  state: LeaseState;
  mac: string;
  host: string | undefined;
  gateway: Gateway;
  range: {
    start: string,
    limit: number
  }
}

interface LeaseProviderOutputs extends LeaseProviderInputs {
  address: string
  validIps: string[]
}

interface DhcpHost {
  mac: string | undefined;
  name: string | undefined;
  ip: string | undefined;
  index: string | undefined;
}
// This is the path to the remote DHCP Lua script
const remoteDhcpLuaScriptPath = '/tmp/dhcp.lua'
class LeaseProvider implements pulumi.dynamic.ResourceProvider {
  async create (inputs: LeaseProviderInputs): Promise<CreateResult> {
    const id = inputs.mac ?? 'no MAC defined';

    const ssh = new NodeSSH();

    try {
      await ssh.connect(inputs.gateway);
      await ssh.putFile(dhcpLuaScriptPath, remoteDhcpLuaScriptPath);
    
      const lockfile = '/tmp/pulumi-create-lock'

      // Create a lock file if it doesn't exist and wait if it does
      await ssh.execCommand(`while [ -e ${lockfile} ]; do sleep 1; done; touch ${lockfile}`);
    
      const dhcpHosts: DhcpHost[] = JSON.parse((await ssh.execCommand(`lua ${remoteDhcpLuaScriptPath}`)).stdout);
      console.log('Existing Leases:', JSON.stringify(dhcpHosts));
    
      // Get the start IP and limit from the range
      const { start, limit } = inputs.range;
    
      // Initialize an array to store the valid IPs
      let addressesRange = [];
    
      // Loop from 0 to limit
      for (let i = 0; i < limit; i++) {
        // Use the ip package to increment the start IP by i and add it to the addressesRange array
        addressesRange.push(ip.fromLong(ip.toLong(start) + i));
      }
    
      // Filter out the IPs that are already in use
      const validIps = addressesRange.filter(ip => !dhcpHosts.some(host => host.ip === ip));
    
      // Now validIps contains the list of valid IPs from start to limit that are not already in use
      console.log('Valid IPs:', validIps);
    
      // Log the IPs of dhcpHosts
      console.log('DHCP Hosts IPs:', dhcpHosts.map(host => host.ip));
    
      const address = validIps.shift()
    
      if (!address) throw new Error('No available addresses, try assigning a larger range for Pulumi to be aware of')
    
      // Create a new DHCP host
      await ssh.execCommand(`uci add dhcp host; uci set dhcp.@host[-1].ip=${address}; uci set dhcp.@host[-1].mac=${inputs.mac}; uci set dhcp.@host[-1].name=${inputs.host}; uci commit dhcp`);
    
      // Wait until dnsmasq server is up and running, then restart it, to avoid simultaneous restarts from parallel lease requests
      await ssh.execCommand('/etc/init.d/dnsmasq reload');

      // Remove the lock file
      await ssh.execCommand(`rm ${lockfile}`);
      
      const outs: LeaseProviderOutputs = {
        ...inputs,
        state: LeaseState.Assigned,
        address,
        validIps
      };
    
      return {
        id,
        outs,
      };
    } catch (err) {
      console.error('SSH Error:', err);
      throw err;
    } finally {
      ssh.dispose();
    }
  }

async delete(id: string, props: LeaseProviderInputs) {
    const ssh = new NodeSSH();

    try {
      await ssh.connect(props.gateway);

      // Retrieve existing DHCP leases
      const dhcpHosts: DhcpHost[] = JSON.parse((await ssh.execCommand(`lua ${remoteDhcpLuaScriptPath}`)).stdout);
      console.log('Existing Leases:', JSON.stringify(dhcpHosts));

      // Find the index of the host to be deleted
      const hostToDelete = dhcpHosts.find(host => host.mac === id);
      if (!hostToDelete) throw new Error('Host not found');
      const indexToDelete = hostToDelete.index;

      // Delete the DHCP host configuration
      await ssh.execCommand(`uci delete dhcp.@host[${indexToDelete}]; uci commit dhcp`);

      // Remove active leases
      await ssh.execCommand(`sed -i '/${hostToDelete.name}/d' /tmp/dhcp.leases`);
      await ssh.execCommand(`sed -i '/${hostToDelete.name}/d' /tmp/dhcp.leases.dynamic`);

      // Reload dnsmasq configuration
      await ssh.execCommand('/etc/init.d/dnsmasq reload');

      console.log(`Deleted DHCP host ${indexToDelete} with id (MAC) ${id}`);
    } catch (err) {
      console.error('SSH Error:', err);
    } finally {
      ssh.dispose();
    }
  }
}

export interface LeaseResourceArgs {
  state: LeaseState;
  mac: pulumi.Input<string | undefined>;
  host: pulumi.Input<string | undefined>;
  gateway: pulumi.Input<Gateway>;
  range?: {
    start: string,
    limit: number
  }
}

export class Lease extends pulumi.dynamic.Resource {
  public readonly address!: pulumi.Output<string>;
  public mac!: pulumi.Output<string>;
  public host!: pulumi.Output<string>;

  constructor(name: string, args: LeaseResourceArgs, opts?: pulumi.CustomResourceOptions) {
    super(new LeaseProvider(), name, { ...args, address: undefined }, opts);
  }
}

export const getRandomMacFromName = (seed: string) => {
  const rng = seedrandom(seed);
  return `54:52:00${Array.from({length: 9}, (_, i) => i%3 === 0 ? ':' : Math.floor(rng()*16).toString(16)).join('')}`.toUpperCase();
}
