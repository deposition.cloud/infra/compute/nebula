local uci = require("luci.model.uci").cursor()
local json = require("luci.json")

local dhcp_hosts = {}
local i = 0

uci:foreach("dhcp", "host", function(s)
    table.insert(dhcp_hosts, {
        name = s.name,
        mac = s.mac,
        ip = s.ip,
        index = i
    })
    i = i + 1
end)

print(json.encode(dhcp_hosts))