
import * as pulumi from "@pulumi/pulumi";
import { Lease, LeaseState, Gateway } from "./lease"

export interface OpenWRTArgs {
  range: {
    start: string,
    limit: number
  }, 
  gateway: Gateway
}

export class OpenWRT extends pulumi.ComponentResource {
  private leases: Lease[] = []

  public gateway: pulumi.Input<Gateway>;
  public range: {
    start: string,
    limit: number
  }

  constructor(name: string, args: OpenWRTArgs, opts: pulumi.ComponentResourceOptions) {
    super("nebula:network:Router", name, args, opts);

    this.gateway = args.gateway
    this.range = args.range

    this.registerOutputs({
      leases: this.leases
    })
  }

  public nextLease(mac: string, host: string) {
    return new Lease(mac, {
      host,
      mac,
      state: LeaseState.Requested,
      gateway: this.gateway,
      range: this.range
    })
  }
}
